var board = '';
for (var i = 0; i < 8; i++) {
	for (var j = 0; j < 8; j++) {
		if (i % 2 === 0 && j % 2 === 0 || i % 2 !== 0 && j % 2 !== 0) {
			board += '\u2B1B';
		} else {
			board += '\u2B1C';
		}
	}
	board += '\n';
}
console.log(board);